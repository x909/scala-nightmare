package controllers

import javax.inject._
import models._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class BasketProductController @Inject()( basketRepo: BasketRepository, productRepo: ProductRepository,
                                         basketProductRepo: BasketProductRepository,
                                        cc: MessagesControllerComponents
                                )(implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  /**
   * The mapping for the person form.
   */
  val basketProductForm: Form[CreateBasketProductForm] = Form {
    mapping(
    "product_id" -> number,
      "basket_id" -> number,
    "quantity" -> number
    )(CreateBasketProductForm.apply)(CreateBasketProductForm.unapply)
  }

  val delBasketProductForm: Form[DeleteBasketProductForm] = Form {
    mapping(
      "product_id" -> number,
      "basket_id" -> number,
      "quantity" -> number
    )(DeleteBasketProductForm.apply)(DeleteBasketProductForm.unapply)
  }



  /**
   * The index action.
   */
  def index = Action.async { implicit request =>
    basketRepo.list().flatMap{ omg=>
     productRepo.list().map {
        x => {
         Ok(views.html.basketproduct(basketProductForm, x, omg))
       }
     }
     }


  }

  def del = Action.async { implicit  request =>

    delBasketProductForm.bindFromRequest.fold(


      errorForm => {
        Future.successful(
          Redirect(routes.BasketController.index).flashing("success" -> "basketProduct.deleted - fucked")

        )
      },
      // There were no errors in the from, so create the person.
      basketProduct => {
        basketProductRepo.del(basketProduct.basket_id, basketProduct.product_id, basketProduct.quantity).map { _ =>
          // If successful, we simply redirect to the index page.
          Redirect(routes.BasketProductController.index).flashing("success" -> "basketProduct.deleted")
        }
      }

    )
  }


  /**
   * The add person action.
   *
   * This is asynchronous, since we're invoking the asynchronous methods on PersonRepository.
   */
/*
  def addProduct = Action.async { implicit request =>
    Ok(views.html.addproduct())
  }
*/

  def add = Action.async { implicit request =>
    // Bind the form first, then fold the result, passing a function to handle errors, and a function to handle succes.
    var a:Seq[BasketProduct] = Seq[BasketProduct]()
    val basketProducts = basketProductRepo.list().onComplete{
      case Success(cat) => a= cat
      case Failure(_) => print("fail")
    }

    basketProductForm.bindFromRequest.fold(
      // The error function. We return the index page with the error form, which will render the errors.
      // We also wrap the result in a successful future, since this action is synchronous, but we're required to return
      // a future because the person creation function returns a future.
      errorForm => {
        Future.successful(
          Redirect(routes.BasketProductController.index).flashing("success" -> "Fucked")
          )
      },
      // There were no errors in the from, so create the person.
      basketProduct => {
        basketProductRepo.create(basketProduct.basket_id, basketProduct.product_id, basketProduct.quantity).map { _ =>
          // If successful, we simply redirect to the index page.
          Redirect(routes.BasketProductController.index).flashing("success" -> "basketProduct.created")
        }
      }
    )
  }


  /**
   * A REST endpoint that gets all the people as JSON.
   */
  def getAll = Action.async { implicit request =>
    basketProductRepo.list().map { basketProducts =>
      Ok(Json.toJson(basketProducts))
    }
  }

  def get(basket_id: String) = Action.async { implicit request =>

    basketProductRepo.get(basket_id.toInt).map{basketJoin =>
      Ok(Json.toJson(basketJoin))
    }

  }
}


/**
 * The create person form.
 *
 * Generally for forms, you should define separate objects to your models, since forms very often need to present data
 * in a different way to your models.  In this case, it doesn't make sense to have an id parameter in the form, since
 * that is generated once it's created.
 */
case class CreateBasketProductForm( product_id: Int, basket_id: Int, quantity: Int)
case class DeleteBasketProductForm( product_id: Int, basket_id: Int, quantity: Int)