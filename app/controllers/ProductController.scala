package controllers

import javax.inject._
import models._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class ProductController @Inject()(productsRepo: ProductRepository,
                                  cc: MessagesControllerComponents
                                )(implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  /**
    * The mapping for the person form.
    */
  val productForm: Form[CreateProductForm] = Form {
    mapping(
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "price" -> number

    )(CreateProductForm.apply)(CreateProductForm.unapply)
  }

  val delProductForm: Form[DeleteProductForm] = Form {
    mapping(
      "id" -> number
    )(DeleteProductForm.apply)(DeleteProductForm.unapply)
  }


  /**
    * The index action.
    */
  def index = Action { implicit request =>

    Ok(views.html.product(productForm))

    /*
      .onComplete{
      case Success(categories) => Ok(views.html.index(productForm,categories))
      case Failure(t) => print("")
    }*/
  }

  /**
    * The add person action.
    *
    * This is asynchronous, since we're invoking the asynchronous methods on PersonRepository.
    */
  /*
  def addProduct = Action.async { implicit request =>
    Ok(views.html.addproduct())
  }
*/

  def delProduct = Action.async { implicit request =>

    delProductForm.bindFromRequest.fold(


      errorForm => {
        Future.successful(
          Redirect(routes.ProductController.index).flashing("success" -> "product.deleted - fucked")

        )
      },
      // There were no errors in the from, so create the person.
      product => {
        productsRepo.del(product.id).map { _ =>
          // If successful, we simply redirect to the index page.
          Redirect(routes.ProductController.index).flashing("success" -> "product.deleted")
        }
      }

    )
  }

  def addProduct = Action.async { implicit request =>
    // Bind the form first, then fold the result, passing a function to handle errors, and a function to handle succes.


    productForm.bindFromRequest.fold(
      // The error function. We return the index page with the error form, which will render the errors.
      // We also wrap the result in a successful future, since this action is synchronous, but we're required to return
      // a future because the person creation function returns a future.
      errorForm => {
        Future.successful(
          Ok(views.html.product(errorForm))
        )
      },
      // There were no errors in the from, so create the person.
      product => {
        productsRepo.create(product.name, product.description, product.price).map { _ =>
          // If successful, we simply redirect to the index page.
          Redirect(routes.ProductController.index).flashing("success" -> "product.created")
        }
      }
    )
  }


  /**
    * A REST endpoint that gets all the people as JSON.
    */
  def getProducts = Action.async { implicit request =>
    productsRepo.list().map { products =>
      Ok(Json.toJson(products))
    }
  }

  def getProductsOpinions(id: String) = Action.async { implicit request =>
    productsRepo.getOpinions(id.toInt).map { products =>

      Ok(Json.toJson(products))
    }
  }




}
/**
 * The create person form.
 *
 * Generally for forms, you should define separate objects to your models, since forms very often need to present data
 * in a different way to your models.  In this case, it doesn't make sense to have an id parameter in the form, since
 * that is generated once it's created.
 */
case class CreateProductForm(name: String, description: String, price: Int)
case class DeleteProductForm(id: Int)