package controllers

import javax.inject._
import models._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class ProductTypeController @Inject()(productRepo: ProductRepository, productTypeRepo: ProductTypeRepository,
                                        cc: MessagesControllerComponents
                                )(implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  /**
   * The mapping for the person form.
   */
  val productTypeForm: Form[CreateProductTypeForm] = Form {
    mapping(
      "product_id" -> number,
      "product_type" -> nonEmptyText
    )(CreateProductTypeForm.apply)(CreateProductTypeForm.unapply)
  }

  val delProductTypeForm: Form[DeleteProductTypeForm] = Form {
    mapping(
      "id" -> number
    )(DeleteProductTypeForm.apply)(DeleteProductTypeForm.unapply)
  }

  /**
   * The index action.
   */
  def index = Action.async { implicit request =>
    val product = productRepo.list()
        product.map(p => Ok(views.html.producttype(productTypeForm, p)))

      /*
      .onComplete{
      case Success(categories) => Ok(views.html.index(productForm,categories))
      case Failure(t) => print("")
    }*/
  }

  /**
   * The add person action.
   *
   * This is asynchronous, since we're invoking the asynchronous methods on PersonRepository.
   */
/*
  def addProduct = Action.async { implicit request =>
    Ok(views.html.addproduct())
  }
*/

def del = Action.async { implicit request =>


  delProductTypeForm.bindFromRequest.fold(

    // The error function. We return the index page with the error form, which will render the errors.
    // We also wrap the result in a successful future, since this action is synchronous, but we're required to return
    // a future because the person creation function returns a future.
    errorForm => {
      Future.successful(
        Redirect(routes.ProductTypeController.index).flashing("success" -> "delete Fucked")

      )
    },
    // There were no errors in the from, so create the person.
    productType => {
      productTypeRepo.del(productType.id).map { _ =>
        // If successful, we simply redirect to the index page.
        Redirect(routes.ProductTypeController.index).flashing("success" -> "producttype.deleted")
      }
    }
  )
}




  def add = Action.async { implicit request =>
    // Bind the form first, then fold the result, passing a function to handle errors, and a function to handle succes.
    var a:Seq[ProductType] = Seq[ProductType]()
    val productType = productTypeRepo.list().onComplete{
      case Success(cat) => a= cat
      case Failure(_) => print("fail")
    }


  productTypeForm.bindFromRequest.fold(

      // The error function. We return the index page with the error form, which will render the errors.
      // We also wrap the result in a successful future, since this action is synchronous, but we're required to return
      // a future because the person creation function returns a future.
      errorForm => {
        Future.successful(
          Redirect(routes.ProductTypeController.index).flashing("success" -> "Fucked")

        )
      },
      // There were no errors in the from, so create the person.
      productType => {
        productTypeRepo.create(productType.product_id, productType.product_type).map { _ =>
          // If successful, we simply redirect to the index page.
          Redirect(routes.ProductTypeController.index).flashing("success" -> "producttype.created")
        }
      }
    )
  }


  /**
   * A REST endpoint that gets all the people as JSON.
   */
  def get = Action.async { implicit request =>
    productTypeRepo.list().map { o =>
      Ok(Json.toJson(o))
    }
  }
}

/**
 * The create person form.
 *
 * Generally for forms, you should define separate objects to your models, since forms very often need to present data
 * in a different way to your models.  In this case, it doesn't make sense to have an id parameter in the form, since
 * that is generated once it's created.
 */
case class CreateProductTypeForm(product_id: Int, product_type: String)
case class DeleteProductTypeForm(id: Int)
