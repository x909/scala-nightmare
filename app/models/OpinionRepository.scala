package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * A repository for people.
  *
  * @param dbConfigProvider The Play db config provider. Play will inject this for you.
  */
@Singleton
class OpinionRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  // We want the JdbcProfile for this provider
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import profile.api._

  /**
    * Here we define the table. It will have a name of people
    */




  class OpinionTable(tag: Tag) extends Table[Opinion](tag, "opinions") {


    /** The ID column, which is the primary key, and auto incremented */
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    /** The name column */
    def product_id = column[Int]("product_id")

    def data = column[String]("data")


    /**
      * This is the tables default "projection".
      *
      * It defines how the columns are converted to and from the Person object.
      *
      * In this case, we are simply passing the id, name and page parameters to the Person case classes
      * apply and unapply methods.
      */
    def * = (id, product_id, data) <> ((Opinion.apply _).tupled, Opinion.unapply)
  }

  /**
    * The starting point for all queries on the people table.
    */


    val opinion = TableQuery[OpinionTable]


  /**
    * Create a person with the given name and age.
    *
    * This is an asynchronous operation, it will return a future of the created person, which can be used to obtain the
    * id for that person.
    */
  def create(product_id: Int, data: String): Future[Opinion] = db.run {
    // We create a projection of just the name and age columns, since we're not inserting a value for the id column
    (opinion.map(c => (c.product_id, c.data))
      // Now define it to return the id, because we want to know what id was generated for the person
      returning opinion.map(_.id)
      // And we define a transformation for the returned value, which combines our original parameters with the
      // returned id
      into { case ((product_id, data), id) => Opinion(id, product_id, data)}
      // And finally, insert the person into the database
      ) += ((product_id, data))
  }

  /**
    * List all the people in the database.
    */
  def list(): Future[Seq[Opinion]] = db.run {
    opinion.result
  }

  def del(del_opinion_id: Int ) = db.run {
    opinion.filter(_.id === del_opinion_id).delete
  }




}
