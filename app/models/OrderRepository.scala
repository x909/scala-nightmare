package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * A repository for people.
  *
  * @param dbConfigProvider The Play db config provider. Play will inject this for you.
  */
@Singleton
class OrderRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  // We want the JdbcProfile for this provider
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import profile.api._



  /**
    * Here we define the table. It will have a name of people
    *
    *
    */
  class OrderTable(tag: Tag) extends Table[Order](tag, "orders") {

    /** The ID column, which is the primary key, and auto incremented */
    //def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    /** The name column */
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def basket_id = column[Int]("basket_id")


    /**
      * This is the tables default "projection".
      *
      * It defines how the columns are converted to and from the Person object.
      *
      * In this case, we are simply passing the id, name and page parameters to the Person case classes
      * apply and unapply methods.
      */
    def * = (id, basket_id) <> ((Order.apply _).tupled, Order.unapply)
  }

  /**
    * The starting point for all queries on the people table.
    */
  private val order = TableQuery[OrderTable]


  /**
    * Create a person with the given name and age.
    *
    * This is an asynchronous operation, it will return a future of the created person, which can be used to obtain the
    * id for that person.
    */

  def create( basket_id: Int): Future[Order] = db.run {

    (order.map(b => (b.basket_id))
      // Now define it to return the id, because we want to know what id was generated for the person
      returning order.map(_.id)
      // And we define a transformation for the returned value, which combines our original parameters with the
      // returned id
      into {case (basket_id, id) => Order(id, basket_id)}
      // And finally, insert the person into the database
      ) += ((basket_id))
  }



  /**
    * List all the people in the database.
    */
  def list(): Future[Seq[Order]] = db.run {
    order.result
  }

  def del(del_order_id: Int ) = db.run {
    order.filter(_.id === del_order_id).delete
  }

}
