package models

import play.api.libs.json._

case class Order(id: Int, basket_id: Int)
object Order {
  implicit val OrderFormat = Json.format[Order]
}
