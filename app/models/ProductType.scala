package models

import play.api.libs.json._

case class ProductType(id: Int, product_id: Int, product_type: String)


object ProductType {
  implicit val ProductTypeFormat = Json.format[ProductType]
}
