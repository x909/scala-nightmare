package models

import play.api.libs.json._

case class BasketProduct(id:Int, product_id: Int, basket_id: Int, quantity: Int)

object BasketProduct {
  implicit val BasketProductFormat = Json.format[BasketProduct]
}
