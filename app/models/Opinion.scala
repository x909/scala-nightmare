package models

import play.api.libs.json._

case class Opinion(id: Int, product_id: Int, data: String)

object Opinion {
  implicit val OpinionFormat = Json.format[Opinion]
}
