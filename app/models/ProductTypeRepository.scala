package models

import akka.http.scaladsl.model.headers.CacheDirectives.public
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * A repository for people.
  *
  * @param dbConfigProvider The Play db config provider. Play will inject this for you.
  */
@Singleton
class ProductTypeRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  // We want the JdbcProfile for this provider
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import profile.api._

  /**
    * Here we define the table. It will have a name of people
    */

  class ProductTypeTable(tag: Tag) extends Table[ProductType](tag, "product_type") {

    /** The ID column, which is the primary key, and auto incremented */
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    /** The name column */
    def product_id = column[Int]("product_id")

    def product_type = column[String]("product_type")


    /**
      * This is the tables default "projection".
      *
      * It defines how the columns are converted to and from the Person object.
      *
      * In this case, we are simply passing the id, name and page parameters to the Person case classes
      * apply and unapply methods.
      */
    def * = (id, product_id, product_type) <> ((ProductType.apply _).tupled, ProductType.unapply)
  }

  /**
    * The starting point for all queries on the people table.
    */
  val productType = TableQuery[ProductTypeTable]


  /**
    * Create a person with the given name and age.
    *
    * This is an asynchronous operation, it will return a future of the created person, which can be used to obtain the
    * id for that person.
    */
  def create(product_id: Int, product_type: String): Future[ProductType] = db.run {
    // We create a projection of just the name and age columns, since we're not inserting a value for the id column
    (productType.map(c => (c.product_id, c.product_type))
      // Now define it to return the id, because we want to know what id was generated for the person
      returning productType.map(_.id)
      // And we define a transformation for the returned value, which combines our original parameters with the
      // returned id
      into { case ((product_id, product_type), id) => ProductType(id, product_id, product_type)}
      // And finally, insert the person into the database
      ) += ((product_id, product_type))
  }

  /**
    * List all the people in the database.
    */
  def list(): Future[Seq[ProductType]] = db.run {
    productType.result
  }

  def del(del_pt_id: Int ) = db.run {
    productType.filter(_.product_id === del_pt_id).delete
  }



}

