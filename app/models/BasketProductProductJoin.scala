package models


package models

import play.api.libs.json._

case class BasketProductProductJoin(basket_id:Int, user_id: String, total_price: Int,  product_id: Int, basket2_id: Int, quantity: Int, id: Int, name: String, description: String, price: Int)

object BasketProductProductJoin {
  implicit val BasketProductProductJoinFormat = Json.format[BasketProductProductJoin]
}
