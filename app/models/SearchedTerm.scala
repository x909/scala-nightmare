package models


import play.api.libs.json._

case class SearchedTerm(id: Int, term: String)
object SearchedTerm {
  implicit val SearchedTermFormat = Json.format[SearchedTerm]
}



