package models
import play.api.libs.json._

case class Pay(id: Int, orders: Int)
object Pay {
  implicit val PayFormat = Json.format[Pay]
}


