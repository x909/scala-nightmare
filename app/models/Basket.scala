package models

import play.api.libs.json._

case class Basket(id: Int, user_id: String, total_price: Int)
object Basket {
  implicit val BasketFormat = Json.format[Basket]
}
