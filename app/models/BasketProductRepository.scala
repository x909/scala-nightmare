package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}
import models._

/**
  * A repository for people.
  *
  * @param dbConfigProvider The Play db config provider. Play will inject this for you.
  */
@Singleton
class BasketProductRepository @Inject()(dbConfigProvider: DatabaseConfigProvider, basketRepository: BasketRepository, productRepository: ProductRepository )(implicit ec: ExecutionContext) {
  // We want the JdbcProfile for this provider
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import profile.api._



  /**
    * Here we define the table. It will have a name of people
    */
class BasketProductTable(tag: Tag) extends Table[BasketProduct](tag, "basket_product") {


    /** The ID column, which is the primary key, and auto incremented */
    //def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    /** The name column */
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def product_id = column[Int]("product_id")
    def basket_id = column[Int]("basket_id")
    def quantity = column[Int]("quantity")


    /**
      * This is the tables default "projection".
      *
      * It defines how the columns are converted to and from the Person object.
      *
      * In this case, we are simply passing the id, name and page parameters to the Person case classes
      * apply and unapply methods.
      */
    def * = (id,product_id, basket_id, quantity) <> ((BasketProduct.apply _).tupled, BasketProduct.unapply)
  }

  /**
    * The starting point for all queries on the people table.
    */

  import basketRepository.BasketTable
  import productRepository.ProductTable

  private val basketProduct = TableQuery[BasketProductTable]
  private val basket = TableQuery[BasketTable]
  private val product = TableQuery[ProductTable]



  /**
    * Create a person with the given name and age.
    *
    * This is an asynchronous operation, it will return a future of the created person, which can be used to obtain the
    * id for that person.
    */

  def create( basket_id: Int, product_id: Int, quantity: Int): Future[BasketProduct] = db.run {

    (basketProduct.map(b => (b.product_id, b.basket_id, b.quantity ))
      // Now define it to return the id, because we want to know what id was generated for the person
      returning basketProduct.map(_.id)
      // And we define a transformation for the returned value, which combines our original parameters with the
      // returned id
      into { case ((product_id, basket_id, quantity), id) => BasketProduct(id, product_id, basket_id, quantity)}
      // And finally, insert the person into the database
      ) += ((product_id, basket_id, quantity))
  }



  /**
    * List all the people in the database.
    */
  def list(): Future[Seq[BasketProduct]] = db.run {
    basketProduct.result
  }

/*
  def get(get_basket_id: Int): Future[Seq[BasketProductJoin]] = db.run {

    basket.filter(_.id === get_basket_id).join(basketProduct).on(_.id === _.basket_id).result.map(s => s.map( a=> new BasketProductJoin(a._1.id, a._1.user_id, a._1.total_price, a._2.product_id, a._2.basket_id, a._2.quantity)))


  }
*/

  def get(in_basket_id: Int): Future[Seq[BasketProductProductJoin]] = db.run {

    basket.filter(_.id=== in_basket_id).join(basketProduct).on(_.id === _.basket_id).join(product).on(_._2.product_id === _.id).
      result.map(s => s.map( a => new BasketProductProductJoin(a._1._1.id, a._1._1.user_id, a._1._1.total_price, a._1._2.product_id, a._1._2.basket_id, a._1._2.quantity, a._2.id, a._2.name, a._2.description, a._2.price)))

  }

  def del(del_basket_id: Int, del_product_id: Int, quantity: Int ) = db.run {
    basketProduct.filter(_.basket_id === del_basket_id).filter(_.product_id === del_product_id).filter(_.quantity === quantity).delete;
  }

}
