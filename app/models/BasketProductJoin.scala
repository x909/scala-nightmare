package models

import play.api.libs.json._

case class BasketProductJoin(basket_id:Int, user_id: String, total_price: Int,  product_id: Int, basket2_id: Int, quantity: Int)

object BasketProductJoin {
  implicit val BasketProductJoinFormat = Json.format[BasketProductJoin]
}
