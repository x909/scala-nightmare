
# --- !Ups
create table "product" (
  "id" integer not null primary key autoincrement,
  "name" varchar not null,
  "description" text not null,
  'price' integer default(0)
);

create table "basket" (
    'id' integer primary key autoincrement,
    'user_id' text,
    "total_price" integer default(0)
);

create table "basket_product" (
    'id' integer primary key autoincrement,
    'product_id' integer not null,
    'basket_id' integer not null,
    'quantity' integer not null default(1),
    foreign key('product_id') references "product"('id') ON UPDATE CASCADE,
    foreign key('basket_id') references "basket"('id') ON UPDATE CASCADE
);

create table "orders" (
    'id' integer primary key autoincrement,
    'basket_id' integer not null,
     foreign key('basket_id') references "basket"('id') on UPDATE CASCADE
);

create table "pays" (
    'id' integer primary key autoincrement,
    'order_id' integer no null,
     foreign key('order_id') references "orders"('id') on UPDATE CASCADE
);

create table "opinions" (
    'id' integer primary key autoincrement,
    'product_id' integer not null,
    'data' varchar not null,
    foreign key('product_id') references "product"('id') ON UPDATE CASCADE
);

create table "product_type" (
    'id' integer primary key autoincrement,
    'product_id' integer not null,
    'product_type' varchar not null,
     foreign key('product_id') references "product"('id') ON UPDATE CASCADE
);

create table "searched_term" (
    'id' integer primary key autoincrement,
    'term' varchar not null
);


create trigger insert_total_price after insert on "basket_product"
begin
    update basket set 'total_price' = (select sum(product.price * basket_product.quantity) as Total_price from basket_product join basket join product where basket_product.product_id = product.id and basket_product.basket_id = basket.id and basket.id = new.basket_id)
    ;;
end
;

create trigger update_total_price after update on "basket_product"
begin
    update basket set 'total_price' = (select sum(product.price * basket_product.quantity) as Total_price from basket_product join basket join product where basket_product.product_id = product.id and basket_product.basket_id = basket.id and basket.id = new.basket_id)
    ;;
end
;


create trigger delete_total_price after delete on basket_product
begin
  update basket set 'total_price' = (select sum(product.price * basket_product.quantity) as Total_price from basket_product join basket join product where basket_product.product_id = product.id and basket_product.basket_id = basket.id and basket.id = old.basket_id)
  ;;
end
;


# --- !Downs
drop table if exists "product";
drop table if exists "basket" ;
drop table if exists "basket_product" ;
drop table if exists "orders" ;
drop table if exists "pays";
drop table if exists "opinions";
drop table if exists "searched_term";

drop trigger if exists insert_total_price ;
drop trigger if exists "update_total_price" ;
